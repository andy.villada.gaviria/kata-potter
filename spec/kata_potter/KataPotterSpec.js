describe("A price calculation", function () {

  beforeEach(function () {
  });

  it("for a single book has no discount", function () {
    var books = [1];
    expect(calculatePrice(books)).toBe(8)
  });

  it("for two different books has 5% discount", function () {
    var books = [1, 2];
    expect(calculatePrice(books)).toBe(15.2)
  });

  it("for three different books has 10% discount", function () {
    var books = [1, 2, 3];
    expect(calculatePrice(books)).toBe(21.6)
  });

  it("for four different book has 20% discount", function () {
    var books = [1, 2, 3, 4];
    expect(calculatePrice(books)).toBe(25.6)
  });

  it("for five different book has 25% discount", function () {
    var books = [1, 2, 3, 4, 5];
    expect(calculatePrice(books)).toBe((8 * 5) * 0.75)
  });

  it("for two different books and a repeated one has 5% discount", function () {
    var books = [1, 2, 1];
    expect(calculatePrice(books)).toBe((8 * 2) * 0.95 + 8)
  });

  it("for two different books and a two repeated has 5% discount", function () {
    var books = [1, 2, 1, 1];
    expect(calculatePrice(books)).toBe((8 * 2) * 0.95 + 8 + 8)
  });

  it("for two sets of different books has 5% discount per set", function () {
    var books = [1, 2, 1, 2];
    expect(calculatePrice(books)).toBe((8 * 2) * 0.95 * 2)
  });

  it("for two large sets of different books has 20% discount per set", function () {
    var books = [1, 1, 2, 2, 3, 3, 4, 5];
    expect(calculatePrice(books)).toBe((8 * 4) * 0.80 * 2)
  });
});

const bookPrice = 8;

function calculatePrice(books) {
  var finalPrice = calculatePriceForSetsOfSize(books, 5);
  
  for (var setSize = 4; setSize >= 1; setSize--) {
    var price = calculatePriceForSetsOfSize(books, setSize);
    if (price < finalPrice) {
      finalPrice = price;
    }
    else {
      break;
    }
  }

  return finalPrice;
}

function calculatePriceForSetsOfSize(books, setSize) {
  var total = 0;
  var allBooks = books.slice();
  do {
    var uniqueBooks = calculateUniqueBooks(allBooks, setSize);
    var fullPrice = calculateFullPrice(uniqueBooks);
    var discount = calculateDiscount(uniqueBooks, fullPrice);
    total += fullPrice - discount;
    removeUniqueBooks(uniqueBooks, allBooks);
  } while (allBooks.length > 0);
  return total;
}

function calculateUniqueBooks(books, maxLength) {
  return books.filter(function (value, index, self) {
    return index == self.indexOf(value);
  }).slice(0, maxLength);
}

function calculateFullPrice(books) {
  return books.length * bookPrice;
}

function calculateDiscount(books, fullPrice) {
  var discountPercentage;
  switch (books.length) {
    case 2:
      discountPercentage = 5;
      break;
    case 3:
      discountPercentage = 10;
      break;
    case 4:
      discountPercentage = 20;
      break;
    case 5:
      discountPercentage = 25;
      break;
    default:
      discountPercentage = 0;
      break;
  }

  return fullPrice * discountPercentage / 100;
}

function removeUniqueBooks(uniqueBooks, books) {
  uniqueBooks.forEach(book => {
    var index = books.indexOf(book);
    books.splice(index, 1);
  });
}